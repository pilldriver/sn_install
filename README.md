Welcome to the SuperNET install script
================
This script provides an easy to use SuperNET installation for Ubuntu 14.04.

_____
## How to use:
~~~~
wget https://bitbucket.org/r3dux/sn_install/downloads/sn_install.sh && bash sn_install.sh
~~~~
