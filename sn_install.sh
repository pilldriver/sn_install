#!/bin/bash

# sn_install.sh - supernet installer.
# 
# DISCLAIMER: USE THIS SCRIPT AT YOUR OWN RISK!
# THE AUTHOR TAKES NO RESPONSIBILITY FOR THE RESULTS OF THIS SCRIPT.
# Disclaimer aside, this worked for the author, for what that's worth.
# 
# Copyright 2014 redux

OPTION=$(whiptail --title "SuperNET Install" --radiolist "This is the SuperNET installation script. \
It will install a privacyserver with all it's requirements. \
Please choose your option below." 15 60 5 \
"1" "INSTALL SuperNET essentials" ON \
"2" "UPDATE SuperNET" OFF \
"3" "UPDATE NXT Client" OFF \
"4" "INSTALL InstantDEX" OFF \
"5" "ENTER Matrix" OFF \
3>&1 1>&2 2>&3)
 
if [[ $EUID -eq 0 ]]; then
	RED='\033[0;31m'
	NC='\033[0m'
	echo -e "${RED}This script should not be run as the root user.${NC}"
	exit 1
fi

sudo echo "Welcome, please follow instructions and proceed."

################################################
#Installation of supernet and other requirements
################################################
SERVERIP=`hostname -I | cut -f1 -d' '` 
SERVERNAME="SuperNET"
USERNAME=`whoami`

if [[ $OPTION == "1" ]]; then

if [ -d ~/srv/ ]; then
	echo "You have already a ~/srv directory."
	read -p "Do you want to delete this? " -n 1 -r 
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		#Check if old Daemons are running
		pgrep -f SuperNET | xargs kill -9
		pgrep -f BitcoinDarkd | xargs kill -9
		pgrep -f nxt.jar | xargs kill -9
		sleep 2
		rm -Rf ~/srv
	else
		exit 1
	fi
fi

echo "You are about to install the SuperNET essentials."

#Installing general requirements
sudo apt-get -y install software-properties-common python-software-properties
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install build-essential make automake unzip git ntp gcc g++ clang cmake autoconf pkg-config autotools-dev 
sudo apt-get -y install libtool libdb++-dev libssl-dev libboost-all-dev libcurl4-gnutls-dev cmatrix miniupnpc libminiupnpc-dev
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
sudo apt-get -y install oracle-java8-installer oracle-java8-set-default

#Starting NXT SuperNET client project
cd ~
mkdir srv
cd ~/srv
		
echo " * Cloning NXT SuperNET Client"
git clone https://bitbucket.org/longzai1988/supernet.git sn_nxt
sleep 2
cd sn_nxt
./compile.sh
chmod +x run.sh

echo " * Creating nxt.properties config file"
cd conf 
echo "nxt.myAddress=$SERVERIP" > nxt.properties
echo "nxt.myPlatform=$SERVERNAME" >> nxt.properties
echo "nxt.allowedBotHosts=127.0.0.1; localhost; $SERVERIP; [0:0:0:0:0:0:0:1];" >> nxt.properties
echo "nxt.allowedUserHosts=127.0.0.1; localhost; $SERVERIP; [0:0:0:0:0:0:0:1];" >> nxt.properties

echo " * Creating sn_nxt.sh upstart script"
cat <<EOF > ~/srv/sn_nxt.sh
#!/bin/bash
case "\$1" in
	start)
		echo -n " * Starting SuperNXT"
		cd /home/$USERNAME/srv/sn_nxt
		nohup ./run.sh >/dev/null 2>&1 &
		echo "."
		;;
	stop)
		echo -n " * Stopping SuperNXT"
		pgrep -f nxt.jar | xargs kill -9
		echo "."
		;;
	*)
		echo "Usage: ./sn_nxt.sh ( start | stop )"
		exit 1
		;;
esac
EOF
chmod +x ~/srv/sn_nxt.sh

#Starting NXT SuperNET client
~/srv/sn_nxt.sh start

#SuperNET enabled BitcoinDark installation
echo " * Creating SuperNET enabled BitcoinDark"
cd ~/srv
git clone https://github.com/jl777/btcd.git sn_btcd
cd sn_btcd/libjl777
make onetime
make patch
make SuperNET
make btcd
rm -Rf ~/.BitcoinDark/debug.log

echo " * Creating proper SuperNET.conf file"
rm -Rf SuperNET.conf
cp SuperNET.conf.default SuperNET.conf
sed -i '/"debug":2,/a "USESSL":0,' SuperNET.conf
sed -i "s/HOME-FOLDER/\/home\/$USERNAME/g" SuperNET.conf

echo " * Adding whitelist servers SuperNET.conf"
sed -i "s/190.10.10.145/190.10.10.145\", \"167.114.2.94\", \"167.114.2.203\", \
\"167.114.2.206\", \"167.114.2.171\", \"192.99.246.33\", \"167.114.2.204\", \
\"167.114.2.205\", \"192.99.246.20\", \"192.99.212.250\", \"192.99.246.126/g" SuperNET.conf

# Dumping BTCD wallet
echo " * Dumping BTCD wallet"
./BitcoinDarkd >/dev/null 2>&1
sleep 4
./BitcoinDarkd dumpwallet wallet.output
pgrep -f BitcoinDarkd | xargs kill -9

echo " * Replacing BTCDADDRES1-5 SuperNET.conf"
BTCD1=`cat ~/srv/sn_btcd/libjl777/wallet.output | egrep -B1 'addr=' | cut -d " " -f 5 | sed 's/addr=//' | sed -n '2p'`
BTCD2=`cat ~/srv/sn_btcd/libjl777/wallet.output | egrep -B1 'addr=' | cut -d " " -f 5 | sed 's/addr=//' | sed -n '3p'`
BTCD3=`cat ~/srv/sn_btcd/libjl777/wallet.output | egrep -B1 'addr=' | cut -d " " -f 5 | sed 's/addr=//' | sed -n '4p'`
BTCD4=`cat ~/srv/sn_btcd/libjl777/wallet.output | egrep -B1 'addr=' | cut -d " " -f 5 | sed 's/addr=//' | sed -n '5p'`
BTCD5=`cat ~/srv/sn_btcd/libjl777/wallet.output | egrep -B1 'addr=' | cut -d " " -f 5 | sed 's/addr=//' | sed -n '6p'`

sed -i "s/BTCDADDRESS-1/$BTCD1/g" SuperNET.conf
sed -i "s/BTCDADDRESS-2/$BTCD2/g" SuperNET.conf
sed -i "s/BTCDADDRESS-3/$BTCD3/g" SuperNET.conf
sed -i "s/BTCDADDRESS-4/$BTCD4/g" SuperNET.conf
sed -i "s/BTCDADDRESS-5/$BTCD5/g" SuperNET.conf

echo " * Adding addnode in BitcoinDark.conf"
echo "" >> ~/.BitcoinDark/BitcoinDark.conf
echo "addnode=31.220.4.41" >> ~/.BitcoinDark/BitcoinDark.conf
echo "addnode=98.226.66.65" >> ~/.BitcoinDark/BitcoinDark.conf
echo "addnode=192.219.117.63" >> ~/.BitcoinDark/BitcoinDark.conf
echo "addnode=178.20.169.208" >> ~/.BitcoinDark/BitcoinDark.conf

cat <<EOF > ~/srv/sn_btcd.sh
#!/bin/bash

HOMEDIR=/home/$USERNAME/srv/sn_btcd/libjl777
LOG=/home/$USERNAME/.BitcoinDark/SuperNET.out

function startSuperNET {
	cp --backup=numbered \$LOG /tmp/ 2>/dev/null
	cd \$HOMEDIR
	nohup ./BitcoinDarkd 2>&1 > \$LOG 2>&1 &
}

function stopSuperNET {
	cd \$HOMEDIR
	pgrep -f SuperNET | xargs kill -9
	pgrep -f BitcoinDarkd | xargs kill -9
}

case "\$1" in
	start)
		echo -n " * Starting BTCD & SuperNET"
		stopSuperNET
		startSuperNET
		echo "."
		;;
	stop)
		echo -n " * Stopping BTCD & SuperNET"
		stopSuperNET
		echo "."
		;;
	*)
		echo "Usage: ./sn_btcd.sh ( start | stop )"
		exit 1
		;;
esac
EOF
chmod +x ~/srv/sn_btcd.sh

#Starting BTCD SuperNET
~/srv/sn_btcd.sh start

echo " * NXT SuperNET installation complete"
fi

# update supernet
if [[ $OPTION == "2" ]]; then
echo " * Stopping SuperNET"
~/srv/sn_btcd.sh stop
echo " * Updating SuperNET"
cd ~/srv/sn_btcd/libjl777
rm -Rf contacts/ deaddrops/ InstantDEX/ multisig/ 
rm -Rf nodestats/ prices/ private/ public/ telepods/
git pull
make SuperNET
echo " * Starting SuperNET"
~/srv/sn_btcd.sh start
fi

# update nxt
if [[ $OPTION == "3" ]]; then
echo " * Stopping NXT"
~/srv/sn_nxt.sh stop
echo " * Updating NXT"
cd ~/srv/sn_nxt
git pull
./compile.sh
echo " * Starting NXT"
~/srv/sn_nxt.sh start
fi

# instantDEX
if [[ $OPTION == "4" ]]; then
clear
echo "not implemented yet"
sleep 5
exit 1
fi

# enter matrix
if [[ $OPTION == "5" ]]; then
clear
echo "Loading Matrix..."
sudo apt-get -y install cmatrix >/dev/null 2>&1
clear
cmatrix
echo "Goodbye, Mr. Anderson"
fi

# install Opalcoin Daemon
if [[ $OPTION == "6" ]]; then
cd ~/srv	
echo " * Cloning Opalcoin Client"
git clone https://github.com/opalcoin/opalcoin sn_opal
cd sn_opal
git checkout supernet
cd libjl777
sudo make install
sudo apt-get -y update
sudo make onetime
./m_unix
./opalcoind
fi


